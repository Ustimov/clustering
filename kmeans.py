import sys
from sklearn import cluster

if __name__ == '__main__':
    file = open(sys.argv[1])
    data = [[float(num) for num in line.split(' ')] for line in file.readlines()]
    file.close()
    k = 2
    kmeans = cluster.KMeans(n_clusters=k)
    kmeans.fit(data)
    print(kmeans.cluster_centers_)
    labels = list(kmeans.labels_)
    file0 = open(sys.argv[2], 'w')
    file02 = open(sys.argv[4], 'w')
    file1 = open(sys.argv[3], 'w')
    file12 = open(sys.argv[5], 'w')
    for i in range(len(labels)):
        if labels[i] == 0:
            file0.write('{0}\n'.format(data[i][0]))
            file02.write('{0}\n'.format(data[i][1]))
        else:
            file1.write('{0}\n'.format(data[i][0]))
            file12.write('{0}\n'.format(data[i][1]))
    file0.close()
    file02.close()
    file1.close()
    file12.close()
