import sys
import scipy.stats

if __name__ == '__main__':
    file1 = open(sys.argv[1])
    file2 = open(sys.argv[2])
    file3 = open(sys.argv[3])

    ser1 = [float(line) for line in file1.readlines()]
    ser2 = [float(line) for line in file2.readlines()]
    ser3 = [float(line) for line in file3.readlines()]

    file1.close()
    file2.close()
    file3.close()

    print("1:1", scipy.stats.spearmanr(ser1, ser1))
    print("1:2", scipy.stats.spearmanr(ser1, ser2))
    print("1:3", scipy.stats.spearmanr(ser1, ser3))
    print("2:2", scipy.stats.spearmanr(ser2, ser2))
    print("2:3", scipy.stats.spearmanr(ser2, ser3))
    print("3:3", scipy.stats.spearmanr(ser3, ser3))